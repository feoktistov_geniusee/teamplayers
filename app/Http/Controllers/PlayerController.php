<?php

namespace App\Http\Controllers;

use App\Player;
use Illuminate\Http\Request;

/**
 * Class PlayerController
 * @package App\Http\Controllers
 */
class PlayerController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return Player::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'team_id' => $request->get('team_id')
        ]);
    }

    /**
     * @param Request $request
     * @param Player $player
     */
    public function update(Request $request)
    {
        foreach ($request->all() as $player) {
            Player::find($player['id'])->update([
                'first_name' => $player['first_name'],
                'last_name' => $player['last_name'],
                'team_id' => $player['team_id'],
            ]);
        }
    }
}
