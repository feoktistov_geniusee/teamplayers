<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

/**
 * Class TeamController
 * @package App\Http\Controllers
 */
class TeamController extends Controller
{
    /**
     * @return Team[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Team::with('players')->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return Team::create([
            'name' => $request->get('name')
        ]);
    }

    /**
     * @param $name
     * @return Team|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function show($name)
    {
        return Team::with('players')->where('name', $name)->first();
    }
}
