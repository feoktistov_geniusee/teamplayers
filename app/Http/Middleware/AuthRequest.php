<?php

namespace App\Http\Middleware;

class AuthRequest
{
    /**
     * @param $request
     * @param \Closure $next
     * @param null $guard
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        if ($request->header('token') !== config('auth.apiToken')) {
            throw new \Exception('Invalid token');
        }

        return $next($request);
    }
}