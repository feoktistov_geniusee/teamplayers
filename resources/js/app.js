require('./bootstrap');

window.Vue = require('vue');

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

Vue.component('team-component', require('./components/TeamComponent.vue').default);
Vue.component('player-component', require('./components/PlayerComponent.vue').default);

const app = new Vue({
    el: '#app'
});
