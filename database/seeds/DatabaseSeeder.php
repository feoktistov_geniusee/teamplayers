<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Team::class, 10)->create()->each(function ($team) {
            $team->players()->save(factory(App\Player::class)->make());
            $team->players()->save(factory(App\Player::class)->make());
            $team->players()->save(factory(App\Player::class)->make());
            $team->players()->save(factory(App\Player::class)->make());
            $team->players()->save(factory(App\Player::class)->make());
        });
    }
}
